<?php
/** @var \Laravel\Lumen\Routing\Router $router */


use Illuminate\Support\Facades\Auth;

$router->group(['prefix' => 'v1', 'namespace' => 'v1'], function () use ($router) {


    $router->post('login', 'UserController@login');
    $router->post('register', 'UserController@register');

    $router->post('forgot-password', 'UserController@forgot_password');

    $router->group(['middleware' => 'auth'] , function () use ($router) {
        $router->get('/user' , function () {
            return Auth::user();
        });
        $router->post('change-password', 'UserController@change_password');
    });
});

